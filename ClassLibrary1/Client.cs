﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    class Client
    {
        protected String name;
        protected String surname;
        protected Account acc;

        public Account Account
        {
            get => default(Account);
            set
            {
            }
        }

        internal Bank Bank
        {
            get => default(Bank);
            set
            {
            }
        }

        public void setName(String newName)
        {
            name = newName;
        }
        public String getName()
        {
            return name;
        }
        public void setSurname(String newSurname)
        {
            surname = newSurname;
        }
        public String getSurname()
        {
            return surname;
        }
        public Account getAccount()
        {
            return acc;
        }
    }
}
