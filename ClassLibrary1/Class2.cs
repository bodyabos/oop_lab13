﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Account
    {
        private int number;
        public Account(int n)
        {
            number = n;
        }
        public void setNumber(int newNumber)
        {
            number = newNumber;
        }
        public int getNumber()
        {
            return number;
        }

    }
}
